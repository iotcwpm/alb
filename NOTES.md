---
title:
author: 
tags:
---

# SS3 stock assessment WPtmT 2022, [IOTC–2022–WPTmT08(AS)–09]()

- 4 areas, 2 sexes, 4 season, 16 LL CPUEs.
- Candidate: SW (R3) CPUE
- LaA CV=0.06, sensitiovity 0.10
- sigmaR 0.6, not smaller
- CHANGE F16 to mirrpor F15
- Model instability to changes in LF, specially LL3
  - ESS = 5, lambda = 0.1
  - LF not LL lambda = 0
- EXPLORE weighting of LF
  - Model must fit LF well
- GRID: SW / NW CPUE

- F1-4: LLCPUE A1, Q1-4
- F5-8: LLCPUE A2, Q1-4
- F9-12: LLCPUE A3, Q1-4
- F13-16: LLCPUE A4, Q1-4
