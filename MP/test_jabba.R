# test_jabba.R - DESC
# alb/MP/test_jabba.R

# Copyright Iago MOSQUEIRA (WMR), 2022
# Author: Iago MOSQUEIRA (WMR) <iago.mosqueira@wur.nl>
#
# Distributed under the terms of the EUPL-1.2


library(mse)

library(doParallel)
registerDoParallel(25)

# FUNCTIONS

source("utilities.R")

# LOAD data

load("data/om.Rdata")

it <- !duplicated(results$orig)
it <- 1:500

stk <- iter(observations(oem)$stk, it)
idx <- iter(observations(oem)$idx, it)
srr <- iter(sr(om), it)
params(srr) <- FLPar(c(params(srr)[1:3,1,]),
  dimnames=list(params=c("s", "R0", "v"), iter=it))


# --- 1. RUN over all unique OM runs 

ay <- 2021

out <- foreach(i=seq(dim(stk)[6]),
  .errorhandling = "pass") %dopar% {
  cat(i, "- ")
  
  tryCatch(jabba.sa(window(iter(stk, i), end=ay),
      window(iter(idx, i), end=ay), args=list(it=1, ay=2021, y0=1950),
      tracking=FLQuant(dimnames=list(metric='conv.est', year=2018:2040)))$stk,
    error=function(e) NULL)
}

run01 <- list(biom=Reduce(combine, lapply(out, stock)),
  rps=Reduce(combine, lapply(out, function(x) attr(x, "refpts"))))

# --- 2. RUN after forecast @ FMSY

stk <- fwdWindow(window(stk, end=2021), end=2040)

control=as(FLQuants(fbar=expand(FLQuant(c(refpts(om)$FMSY[,it]) * 2, 
    dimnames=list(age='all',  year=2022, iter=seq(dims(stk)$iter))),
    year=2022:2040, fill=TRUE)), "fwdControl")

stk <- fwd(stk, sr=srr, deviances=seasonMeans(unitMeans(residuals(srr))), control=control)

ay <- 2040

out <- foreach(i=seq(dim(stk)[6]),
  .errorhandling = "pass") %dopar% {
  cat(i, "- ")
  
  tryCatch(jabba.sa(window(iter(stk, i), end=ay),
      window(iter(idx, i), end=ay), args=list(it=1, ay=2021, y0=1950),
      tracking=FLQuant(dimnames=list(metric='conv.est', year=2018:2040)))$stk,
    error=function(e) NULL)
}

run02 <- list(biom=Reduce(combine, lapply(out, stock)),
  rps=Reduce(combine, lapply(out, function(x) attr(x, "refpts"))))


# --- 3. RUN after forecast @ FMSY * 1.5

stk <- fwdWindow(window(stk, end=2021), end=2040)

control=as(FLQuants(fbar=expand(FLQuant(c(refpts(om)$FMSY[,it]) * 1.5, 
    dimnames=list(age='all',  year=2022, iter=seq(dims(stk)$iter))),
    year=2022:2040, fill=TRUE)), "fwdControl")

stk <- fwd(stk, sr=srr, deviances=seasonMeans(unitMeans(residuals(srr))), control=control)

ay <- 2040

out<- foreach(i=seq(dim(stk)[6]),
  .errorhandling = "pass") %dopar% {
  cat(i, "- ")
  
  tryCatch(jabba.sa(window(iter(stk, i), end=ay),
      window(iter(idx, i), end=ay), args=list(it=1, ay=2021, y0=1950),
      tracking=FLQuant(dimnames=list(metric='conv.est', year=2018:2040)))$stk,
    error=function(e) NULL)
}

run03 <- list(biom=Reduce(combine, lapply(out, stock)),
  rps=Reduce(combine, lapply(out, function(x) attr(x, "refpts"))))

save(run01, run02, run03, file="test/jabba_runs.Rdata")

# --- RUN MP

control <- mpCtrl(list(
  est = mseCtrl(method=jabba.sa),
  hcr = mseCtrl(method=hockeystick.hcr,
    args=list(lim=0.10, trigger=0.40, target=mean(refpts(om)$MSY) * 0.30,
      metric=relmets$BMSY, output="catch", dlow=0.85, dupp=1.15))))

mp01 <- mp(iter(om, it), oem=iter(oem, it), ctrl=control, args=list(iy=2021))

control <- mpCtrl(list(
  est = mseCtrl(method=jabba.sa),
  hcr = mseCtrl(method=hockeystick.hcr,
    args=list(lim=0.10, trigger=0.40, target=mean(refpts(om)$MSY) * 2,
      metric=relmets$BMSY, output="catch", dlow=0.85, dupp=1.15))))

mp02 <- mp(iter(om, it), oem=iter(oem, it), ctrl=control, args=list(iy=2021))


# --- RUN MP

control <- mpCtrl(list(
  est = mseCtrl(method=jabba.sa),
  hcr = mseCtrl(method=hockeystick.hcr,
    args=list(lim=0.10, trigger=0.40, target=mean(refpts(om)$MSY) * 0.90,
      metric=relmets$B0, output="catch", dlow=0.85, dupp=1.15))))

per01 <- mp(iter(om, it), oem=iter(oem, it), ctrl=control,
  args=list(iy=2021, frq=3))


control <- mpCtrl(list(
  est = mseCtrl(method=jabba.sa),
  hcr = mseCtrl(method=hockeystick.hcr,
    args=list(lim=0.10, trigger=0.40, target=mean(refpts(om)$MSY) * 1.5,
      metric=relmets$B0, output="catch", dlow=0.85, dupp=1.15))))

per02 <- mp(iter(om, it), oem=iter(oem, it), ctrl=control,
  args=list(iy=2021, frq=3))

ssb(per01) / ssb(per02)

# What has rule done?

tracking(per01)['B.est',]


