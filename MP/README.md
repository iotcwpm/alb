---
title:
author: 
tags:
---


# TCMP 2021

- Uncertainty in catch data.
  - Total catch, CaA/CaL.
- Tuning for 11-15 years after OM conditioning.
- TAC constraint 15%
- INVESTIGATE Variable TAC constraints based on current stock status.
- OBJECTIVES: 50%, 60%, and 70% Kobe green
