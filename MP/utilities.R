# utilities.R - DESC
# /utilities.R

# Copyright Iago MOSQUEIRA (WMR), 2021
# Author: Iago MOSQUEIRA (WMR) <iago.mosqueira@wur.nl>
#
# Distributed under the terms of the EUPL-1.2


library(JABBA)

# jabba.sa {{{

#' @examples
#' data(ple4)
#' data(ple4.index)
#' inb <- FLIndices(A=FLIndexBiomass(index=quantSums(index(ple4.index) *
#'   stock.wt(ple4)[, ac(1996:2017)])))
#' system.time(run <- jabba.sa(ple4, inb, args=list(it=1, y0=1957, ay=2017),
#' tracking=FLQuant(dimnames=list(metric='conv.est', year=1950:2020))))

jabba.sa <- function(stk, idx, args, tracking, idx.se=rep(0.2, length(idx)),
  model.type = c("Fox", "Schaefer", "Pella", "Pella_m"), verbose=FALSE) {

  # EXTRACT args
  ay <- args$ay
  it <- args$it
  y0 <- args$y0

  # PREPARE outputs
  B <- stock(stk) %=% an(NA)
  
  refpts <- FLPar(NA,
    dimnames=list(params=c("FMSY", "BMSY", "MSY", "K", "B0"),
    iter=dimnames(stk)$iter), units=c("t", "f", "t", "t", "t"))

  conv <- rep(0, it)

  # LOOP
  for(i in seq(it)) {

    # EXTRACT catch and index
    
    ca <- as.data.frame(iter(catch(stk), i), drop=TRUE)
    id <- model.frame(window(iter(index(idx), i), start=y0), drop=TRUE)
    se <- id

    # ASSIGN idx.se
    se[, -1] <- as.list(idx.se)

    # CONSTRUCT input object
    inp <- build_jabba(catch=ca, cpue=id, se=se,
      assessment="STK", scenario="jabba.sa", model.type=match.arg(model.type),
      sigma.est=FALSE, fixed.obsE=0.05, verbose=verbose)

    # FIT
    fit <- tryCatch(
      mp_jabba(inp),
      # error, RETURN 0 output
      error = function(e) return(list(
        B=as.data.frame(iter(B, i) %=% 9),
        refpts=data.frame(K=9, Bmsy=1, Fmsy=0.0001, MSY=1)))
    )

    # B
    iter(B, i)[] <- as.FLQuant(fit$B)[, - dim(B)[2]]

    # refpts
    iter(refpts, i) <- fit$refpts[c('Fmsy', 'Bmsy', 'MSY', 'K', 'K')]

    # tracking
    if(fit$B[1, "data"] != 9) {
      conv[i] <- 1
    }
    cat(i, " ")
  }

  # STORE outputs: biomass in @stock
  stock(stk) <- B

  # refpts as attribute
  attr(stk, "refpts") <- refpts
  
  # EMPTY stock.n to avoid calls to ssb()
  stock.n(stk) <- as.numeric(NA)

  # TRACK convergence
  track(tracking, "conv.est", ac(ay)) <- conv
  
  list(stk = stk, tracking = tracking)
}
# }}}

# refpts(FLStock) {{{

setMethod("refpts", signature(object="FLStock"),
  function(object) {

    if(is.null(attr(object, "refpts")))
      return(FLPar())
    else
      return(attr(object, "refpts"))
  }
)
# }}}

# metrics {{{

# OVERALL

mets <- list(Rec=function(x) unitSums(rec(x)), SB=function(x) unitSums(ssb(x)),
  C=function(x) unitSums(catch(x)), F=function(x) unitMeans(fbar(x)))

# RELATIVE metrics

relmets <- list(
  SBMSY=function(x) unitSums(ssb(x)) %/% refpts(x)$SBMSY,
  SB0=function(x) unitSums(ssb(x)) %/% refpts(x)$SB0,
  B0=function(x) unitSums(stock(x)) %/% refpts(x)$B0,
  SB1=function(x) unitSums(ssb(x)) %/% unitSums(ssb(x)[,1]),
  BK=function(x) unitSums(stock(x)) %/% refpts(x)$K,
  FMSY=function(x) unitMeans(fbar(x)) %/% refpts(x)$FMSY) 

# }}}

# ---

# jabba.sa {{{

#' @examples
#' data(ple4)
#' data(ple4.index)
#' inb <- FLIndices(A=FLIndexBiomass(index=quantSums(index(ple4.index) *
#'   stock.wt(ple4)[, ac(1996:2017)])))
#' system.time(run <- jabba.sa(ple4, inb, args=list(it=1, y0=1957, ay=2017),
#' tracking=FLQuant(dimnames=list(metric='conv.est', year=1950:2020))))

jabba.sa <- function(stk, idx, args, tracking, idx.se=rep(0.2, length(idx)),
  model.type = c("Fox", "Schaefer", "Pella", "Pella_m"), verbose=FALSE) {

  # EXTRACT args
  ay <- args$ay
  it <- args$it
  y0 <- args$y0

  # PREPARE outputs
  B <- stock(stk) %=% an(NA)
  
  refpts <- FLPar(NA,
    dimnames=list(params=c("FMSY", "BMSY", "MSY", "K", "B0"),
    iter=dimnames(stk)$iter), units=c("t", "f", "t", "t", "t"))

  conv <- rep(0, it)

  # LOOP
  for(i in seq(it)) {

    # EXTRACT catch and index
    
    ca <- as.data.frame(iter(catch(stk), i), drop=TRUE)
    id <- model.frame(window(iter(index(idx), i), start=y0), drop=TRUE)
    se <- id

    # ASSIGN idx.se
    se[, -1] <- as.list(idx.se)

    # CONSTRUCT input object
    inp <- build_jabba(catch=ca, cpue=id, se=se,
      assessment="STK", scenario="jabba.sa", model.type=match.arg(model.type),
      sigma.est=FALSE, fixed.obsE=0.05, verbose=verbose)

    # FIT
    fit <- tryCatch(
      mp_jabba(inp),
      # error, RETURN 0 output
      error = function(e) return(list(
        B=as.data.frame(iter(B, i) %=% 9),
        refpts=data.frame(K=9, Bmsy=1, Fmsy=0.0001, MSY=1)))
    )

    # B
    iter(B, i)[] <- as.FLQuant(fit$B)[, - dim(B)[2]]

    # refpts
    iter(refpts, i) <- fit$refpts[c('Fmsy', 'Bmsy', 'MSY', 'K', 'K')]

    # tracking
    if(fit$B[1, "data"] != 9) {
      conv[i] <- 1
    }
    cat(".")
  }

  # STORE outputs: biomass in @stock
  stock(stk) <- B

  # refpts as attribute
  stk <- FLStockR(stk, refpts=refpts)

  # attr(stk, "refpts") <- refpts
  
  # EMPTY stock.n to avoid calls to ssb()
  stock.n(stk) <- as.numeric(NA)

  # TRACK convergence
  track(tracking, "conv.est", ac(ay)) <- conv
  
  list(stk = stk, tracking = tracking)
}
# }}}
